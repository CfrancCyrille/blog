#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

#FAVICON = 'images/favicon.png'#j'ai pas cet icone !

AUTHOR = u'Cyrille, Techno-Agiliste facilitateur'
#AVATAR = 'images/photo.jpg'#marche pas
SITENAME = u'Ateno blog !'
SITEURL = 'https://cfranccyrille.gitlab.io'

# Les dossiers de Pelican

PATH = 'content' # Le contenu
OUTPUT_PATH = 'public' # Le résultat

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Markup
TYPOGRIFY = True # je vois pas la différence

# Specify name of a built-in theme
THEME = "./pelican-bootstrap3" #ca marche pas c'est à la configuration de pelican pas au niveau du site ?!!!

# On ne génère pas les flux ATOM et RSS
# Mais ce paramètre est remplacé dans publishconf.py

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

ARTICLE_URL = "{slug}/" # Correspondance avec wordpress (pour un modèle url/post/)
ARTICLE_SAVE_AS = "{date:%Y}/{slug}.html" # je range comme je veux

PROFILE_IMAGE = "./images/photo.jpg"
RELATIVE_URLS = True

# Les liens vers vos comptes sociaux

SOCIAL = (('Facebook', 'https://www.facebook.com/cfranc.cyrille'),
          ('Twitter', 'https://twitter.com/CfrancCyrille'),)

DEFAULT_PAGINATION = 5

SITESUBTITLE = u'Le blog du Techno-Agiliste facilitateur'
